
1.Membuat Database
Jawab: create database myshop;

2.Membuat Table di Dalam Database
Jawab : create table users(
id int primary key auto_increment,
name varchar(255),
email varchar(255),
password varchar(255)
);

create table categories(
id int primary key auto_increment,
name varchar(255)
);

create table items(
id int primary key auto_increment,
name varchar(255),
description varchar(255),
price int,
stock int,
category_id int,
foreign key (category_id) references categories(id)
);

3. Memasukkan Data pada Table
Jawab : Insert table users
insert into users(name, email, password) values ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

insert table categories
insert into categories(name) values ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

insert table items
insert into items(name, description, price, stock, category_id) values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand tenama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4. Mengambil Data dari Database
Jawab : a. Mengambil data users
select id, name, email from users;

b. mengambil data items
- Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
select * from items where price > 1000000;

- Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
select * from items where name like 'uniklo%';

c. Menampilkan data items join dengan kategori
select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;


5.Mengubah Data dari Database
Jawab : update items set price = 2500000 where id = 1;

 